package com.example.tabr.client;

import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

/**
 * Composite UiBinder widget
 * @author slynn1324
 */
public class RequestQuoteWidget extends Composite {

	private static RequestQuoteWidgetUiBinder uiBinder = GWT.create(RequestQuoteWidgetUiBinder.class);

	interface RequestQuoteWidgetUiBinder extends UiBinder<Widget, RequestQuoteWidget> {}

	private RequestQuoteViewController controller;

	@UiField
	TextBox what;
	
	@UiField
	TextBox quantity;
	
	@UiField
	DateBox when;
	
	@UiField
	Button reset;
	
	@UiField
	Button submit;
	
	public RequestQuoteWidget() {
		initWidget(uiBinder.createAndBindUi(this));
		
		controller = new RequestQuoteViewController(this);
		
		// set the date format to a short date format
		when.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT)));
		
	}
	
	@UiHandler("reset")
	void onResetClick(ClickEvent e){
		controller.reset();
	}
	
	@UiHandler("submit")
	void onSubmitClick(ClickEvent e){
		controller.submit();
	}
	
	void displayAlert(String msg){
		Window.alert(msg);
	}
	
	void setWhat(String what){
		this.what.setText(what);
	}
	
	void setQuantity(String quantity){
		this.quantity.setText(quantity);
	}
	
	void setWhen(Date when){
		this.when.setValue(when);
	}
	
	/**
	 * By breaking out the functionality into a controller, we separate the implementation details
	 * from the view details. This also makes the controller testable with junit tests.
	 * 
	 * This is not a requirement for this application to work, but it is best practice to separate
	 * the UIs Logic from it's Rendering where possible.
	 * 
	 * @author slynn1324
	 */
	private static class RequestQuoteViewController{
		
		RequestQuoteWidget view;
		
		public RequestQuoteViewController(RequestQuoteWidget view){
			this.view = view;
		}
		
		public void reset(){
			view.setWhat(null);
			view.setQuantity(null);
			view.setWhen(null);
		}
		
		public void submit(){
			view.displayAlert("Thanks!");
		}
	}

}
