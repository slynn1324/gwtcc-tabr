package com.example.tabr.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Tabr implements EntryPoint {
	
	private static final String IMAGE_URI = "gwt-logo.png";
	
	private static final int DECK_HOME = 0;
	private Widget home;
	private static final String HOME_TAB_TEXT = "Home";
	private static final int HOME_TAB = 0;
	private Widget products;
	private static final String PRODUCTS_TAB_TEXT = "Products";
	private static final int PRODUCTS_TAB = 1;
	private Widget contact;
	private static final String CONTACT_TAB_TEXT = "Contact";
	private static final int CONTACT_TAB = 2;
	private Widget quote;
	private static final String QUOTE_TAB_TEXT = "Quote";
	private static final int QUOTE_TAB = 3;
	// private TabPanel tabs;
	private TabLayoutPanel tabs;
	
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		setUpGui();
		setUpHistory();
	}
	
	protected void setUpGui(){
		createLogo();
		createTabs();
	}
	
	/**
	 * Set up the History event handlers
	 * 1) Add a new History token when the tab changes
	 * 2) Handle the history value change event to update the screen when the hash changes
	 */
	protected void setUpHistory(){
		
		tabs.addSelectionHandler(new SelectionHandler<Integer>(){

			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				String loc = null;
				if ( event.getSelectedItem() == HOME_TAB ){
					loc = HOME_TAB_TEXT;
				} else if ( event.getSelectedItem() == PRODUCTS_TAB){
					loc = PRODUCTS_TAB_TEXT;
				} else if ( event.getSelectedItem() == CONTACT_TAB){
					loc = CONTACT_TAB_TEXT;
				} else if ( event.getSelectedItem() == QUOTE_TAB){
					loc = QUOTE_TAB_TEXT;
				}
				if ( loc != null ){
					History.newItem(loc);
				}
			}
			
		});
		
		// some people prefer to make their class implement ValueChangeHandler, and
		// then register itself by History.addValueChangeHandler(this) rather than
		// creating an anonymous inner class. Personal preference here in most cases.
		History.addValueChangeHandler(new ValueChangeHandler<String>(){

			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				String loc = event.getValue() != null ? event.getValue().trim() : "";
				if ( HOME_TAB_TEXT.equals(loc) || "".equals(loc) ){
					tabs.selectTab(HOME_TAB);
				} else if ( PRODUCTS_TAB_TEXT.equalsIgnoreCase(loc) ){
					tabs.selectTab(PRODUCTS_TAB);
				} else if ( CONTACT_TAB_TEXT.equalsIgnoreCase(loc) ){
					tabs.selectTab(CONTACT_TAB);
				} else if ( QUOTE_TAB_TEXT.equalsIgnoreCase(loc) ){
					tabs.selectTab(QUOTE_TAB);
				} else {
					GWT.log("Unknown location: " + loc);
					tabs.selectTab(HOME_TAB);
				}
			}
			
		});
		
		// start it in the right place
		History.fireCurrentHistoryState();
	}
	
	/**
	 * Create the logo image and add it to the 'logo' div.
	 */
	protected void createLogo(){
		Image img = new Image(IMAGE_URI);
		img.setHeight("50px");
		img.setWidth("50px");
		RootPanel logoSlot = RootPanel.get("logo");
		if ( logoSlot != null ){
			logoSlot.add(img);
		}
	}
	
	/**
	 * Create a TabPanel and content
	 */
	protected void createTabs(){
		tabs = new TabLayoutPanel(50, Unit.PX);
		tabs.getElement().getStyle().setMarginTop(50, Unit.PX);
		RootLayoutPanel tabSlot = RootLayoutPanel.get();
		// tabs = new TabPanel();
		//RootPanel tabSlot = RootPanel.get("tabs");
		if ( tabSlot != null ){
			tabSlot.add(tabs);
		}
		
		createTabContent();	
	}
	
	/**
	 * Create the 4 tabs, and select the home tab to start
	 */
	protected void createTabContent(){
		
		createHomeTab();
		createProductsTab();
		createContactTab();
		createQuoteTab();
		
		tabs.selectTab(DECK_HOME);
	}
	
	/**
	 * Create a widget by wrapping html in the underlying page
	 * @return the home page content
	 */
	protected void createHomeTab(){
		home = new HTMLPanel(getContent("home"));
		tabs.add(home, HOME_TAB_TEXT);
	}
	
	/**
	 * Create a widget by combining widgets into a panel
	 * @return the products page content
	 */
	protected void createProductsTab(){
		VerticalPanel vp = new VerticalPanel();
		vp.add(new HTML("Planes"));
		vp.add(new HTML("Trains"));
		vp.add(new HTML("Automobiles"));
		products = vp;
		tabs.add(products, PRODUCTS_TAB_TEXT);
	}
	
	/**
	 * Create a ContactWidget and put on a tab
	 */
	protected void createContactTab(){
		contact = new ContactWidget();
		tabs.add(contact, CONTACT_TAB_TEXT);
	}
	
	/**
	 * Create a RequestQuoteWidget (UiBinder) and put on the tab
	 */
	protected void createQuoteTab(){
		quote = new RequestQuoteWidget();
		tabs.add(quote, QUOTE_TAB_TEXT);
	}
	
	/**
	 * Extract the innerHTML of an element on the HTML page.
	 * @param id - the id attribute of the DOM element to extract content from
	 * @return the innerHTML string
	 */
	protected String getContent(String id){
		String content;
		RootPanel rp = RootPanel.get(id);
		if ( rp != null ){
			content = rp.getElement().getInnerHTML();
		} else {
			content = "";
		}
		return content;
	}
}
