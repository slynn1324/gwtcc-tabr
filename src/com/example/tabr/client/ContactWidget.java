package com.example.tabr.client;

import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Simple Composite widget that combines the elements that make up the Contact tab
 * @author slynn1324
 */
public class ContactWidget extends Composite{

	private VerticalPanel content;
	private HTML html;
	private Anchor mailto;
	
	public ContactWidget(){
		html = new HTML("<b>Address</b>Tabr<br />123 Rodeo Dr.<br />Beverly Hills, CA 90210<br /><br /><b>Phone</b><br />867-5309");
		mailto = new Anchor("example@tabr.com", "mailto://example@tabr.com");
		
		content = new VerticalPanel();
		content.add(html);
		content.add(mailto);
		
		// we MUST call this to set the base widget for this composite
		initWidget(content);
	}
}
